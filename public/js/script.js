/* Drop Down Menu */
function dropdownMenu(winWidth) {

   if (winWidth > 767) {

      $('.main-menu li.drop-down').on('mouseover', function () {
         var $this = $(this),
            menuAnchor = $this.children('a');

         menuAnchor.addClass('mouseover');

      }).on('mouseleave', function () {
         var $this = $(this),
            menuAnchor = $this.children('a');

         menuAnchor.removeClass('mouseover');
      });

   } else {

      $('.main-menu li.drop-down > a').on('click', function () {

         if ($(this).attr('href') == '#') return false;
         if ($(this).hasClass('mouseover')) { $(this).removeClass('mouseover'); }
         else { $(this).addClass('mouseover'); }
         return false;
      });
   }

}

function isExists(elem) {
   if ($(elem).length > 0) {
      return true;
   }
   return false;
}