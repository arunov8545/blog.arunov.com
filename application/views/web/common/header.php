<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="utf-8">
	<title>Welcome to OV's Blog</title>
	<link rel="preconnect" href="https://blog.arunov.com"> 
	<link rel="stylesheet"  href="<?= base_url('public/vendor/bootstrap_5_0_beta1/css/bootstrap.min.css'); ?>" /> 
	<link rel="stylesheet"  href="<?= base_url('public/css/style.css'); ?>" /> 
	
</head>
<body>
 	<?php  $this->load->view('web/common/navbar'); ?>