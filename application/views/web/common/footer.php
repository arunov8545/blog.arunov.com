
      <!-- Predefined Scriots -->
      <script src="<?= base_url('public/vendor/jquery/jquery-3.5.1.min.js'); ?>"></script> 
      <script src="<?= base_url('public/vendor/bootstrap_5_0_beta1/js/bootstrap.bundle.min.js'); ?>"></script> 
      <script src="<?= base_url('public/vendor/jquery/jquery-migrate-3.3.2.js'); ?>"></script> 

      <!-- Custom Scripts -->
      <script src="<?= base_url('public/js/script.js'); ?>"></script>

      <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
   </body>
</html>